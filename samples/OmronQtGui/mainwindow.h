#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
#include <QTimer>

#include "plcqlib.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    //Translate GUI
    QTranslator *translator;
    void translateTo( QString);
    void changeEvent(QEvent *e);

    //PLC
    PlcProxy                *plc1Proxy;
    PlcEventHandler         plc1EventHandler;    //proxy to widgets
    PlcWidgetEventHandler   widget1EventHandler; //widgets to proxy

    //GUI
    QList<PlcWidgetSender*> pwsList;            //registrated senders objects
    void initPlc1WidgetsStructure();
    void startPlc1Comm();

    //private var
    bool disable_shutdown;
    QTimer *timerPer1000;
    QTimer *plc1RestartTimer;


private slots:
    void transtateToEnglish() {translateTo("en");}
    void translateToSlovak() {translateTo("sk");}

    void adminLogin();
    void adminLogout();

    void initMainWindow();
    void plc1ProxyError( PlcProxy::EplcProxyError error);

    void shutMeDown();

    void checkFullScreen();

    void mainTabChanged( int);

    void plc1Restart();

    void periodic1000ms();

};

#endif // MAINWINDOW_H
